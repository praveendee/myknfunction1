from parliament import Context
from datetime import datetime

 
def main(context: Context):
    """ 
    Function template
    The context parameter contains the Flask request object and any
    CloudEvent received with the request.
    """
    return { "message": "Hello OpenSON Team, current time is "+datetime.now().strftime("%d/%m/%Y %H:%M:%S") }, 200
